import { Client } from "./client";
import { State } from "./state";
import { Message } from "./messages";

export abstract class BaseTransport {
    client: Client
    lastMessageId: number | undefined
    state: State
    constructor(client: Client) {
        this.client = client
        this.lastMessageId = undefined
        this.state = State.DISCONNECTED
    }
    abstract start(): void
    abstract stop(): void
    abstract sendMessage(message: Message): void
}
