import { Signal } from "./signal"
import { Exception, TransportNegotiationFailed, AuthenticationFailed, ConnectionError, RequestFailed } from "./exceptions"
import { Message, RequestMessage, NotificationMessage, ResponseMessage, ErrorMessage, HeartbeatMessage, DisconnectMessage } from "./messages"
import { MessageQueue } from "./messagequeue"
import { post } from "./xhr"
import { BaseTransport } from "./basetransport"
import { State } from "./state";
import { PollTransport } from "./polltransport";
import { WebSocketTransport } from "./wstransport";

/**
 * This field defines whether WebSocket support is available or not.
 */
var WEB_SOCKET_SUPPORT = 'WebSocket' in window

export type Method = (data: any) => Promise<any> | undefined

export type TransportName = 'ws'|'poll'

interface ClientOptions {
    url: string,
    transports: TransportName[]
    methods: {
        [name: string]: Method
    }
    queue: MessageQueue
    requests: {
        [messageId: number]: RequestMessage
    }
}

/**
 * The mushroom client class.
 * @param {Object} options
 * @constructor
 */
export class Client {
    url: string
    signals: {
        error: Signal<Exception>
        connected: Signal<void>
        disconnected: Signal<void>
    }
    transports: string[]
    transport: BaseTransport | undefined
    methods: {
        [name: string]: Method
    }
    queue: MessageQueue
    requests: {
        [messageId: number]: RequestMessage
    }
    constructor(options: ClientOptions) {
        this.url = options.url
        this.transports = options.transports ||
                (WEB_SOCKET_SUPPORT ? ['ws', 'poll'] : ['poll'])
        this.transport = undefined
        this.methods = options.methods || {}
        this.queue = new MessageQueue()
        this.requests = {}

        this.signals = {
            // This signal is sent when a request returns an error and
            // no errorCallback was specified.
            error: new Signal(),
            // This signal is sent when the connection is established.
            connected: new Signal(),
            // This signal is sent when the connection was terminated.
            disconnected: new Signal()
        }

        // There is no guarantee that the disconnect function is ever
        // called when the browser window is closed. It is worth a try
        // nonetheless.
        window.addEventListener('beforeunload', () => this.disconnect())
    }
    connect(auth: any) {
        var request = {
            transports: this.transports,
            auth: auth || undefined
        }
        post(this.url, request)
            .then(xhr => {
                var jsonResponse = JSON.parse(xhr.responseText)
                switch (jsonResponse.transport) {
                    case 'poll':
                        this.transport = new PollTransport(this, jsonResponse)
                        break
                    case 'ws':
                        this.transport = new WebSocketTransport(this, jsonResponse)
                        break
                    default:
                        this.signals.error.send(new TransportNegotiationFailed(
                            'Unsupported transport ' + this.transport))
                        return
                }
                this.transport!.start()
            })
            .catch(xhr => {
                if (xhr.status === 401) {
                    this.signals.error.send(new AuthenticationFailed(
                            'Authentication failed'))
                } else {
                    this.signals.error.send(new ConnectionError(
                            'Connection failed. Status code=' + xhr.status))
                }
            })
    }

    /**
     * Disconnect client from server.
     * @throws mushroom.Exception
     */
    disconnect() {
        // FIXME check current state of transport
        this.sendMessage(new DisconnectMessage({
            client: this
        }))
    }

    /**
     * Register a client method
     * @param name {String} name of the method
     * @param callback {Function} callback function which should
     *                            handle method calls for this method name
     */
    method(name: string, callback: Method) {
        this.methods[name] = callback
        return this
    }

    /**
     * Send notification to the server.
     * @param method {String} name of the method to be called
     * @param data {*} data the request data
     */
    notify(method: string, data: any) {
        var notification = new NotificationMessage({
            client: this,
            method: method,
            data: data
        })
        this.queue.push(notification)
        this.sendMessage(notification)
    }

    /**
     * Retrieve request object from requests object while
     * removing it from there. This method is used to retrieve
     * the request object when receiving a response or error.
     */
    popRequest(id: number): RequestMessage {
        var request = this.requests[id]
        delete this.requests[id]
        return request
    }

    /**
     * Send request to the server.
     * @param method {String} name of the method to be called
     * @param data {*} data the request data
     * @param responseCallback {Function} callback which is called
     *     when the response is received.
     * @param errorCallback {Function} callback which is called
     *     when an error response is received. If no callback is
     *     provided the global error signal is used instead.
     */
    request(method: string, data: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            var request = new RequestMessage({
                client: this,
                method: method,
                data: data,
                responseCallback: (data: any) => resolve(data),
                errorCallback: (error: any) => reject(error),
            })
            this.queue.push(request)
            this.requests[request.messageId!] = request
            this.sendMessage(request)
        })
    }

    /**
     * Send message
     * @param message {Object}
     */
    sendMessage(message: Message) {
        if (this.transport !== undefined && this.transport.state === State.CONNECTED) {
            this.transport.sendMessage(message)
        }
    }

    handleMessage(message: Message) {
        if (message instanceof HeartbeatMessage) {
            return this.handleHeartbeat(message)
        }
        if (message instanceof NotificationMessage) {
            return this.handleNotification(message)
        }
        if (message instanceof RequestMessage) {
            return this.handleRequest(message)
        }
        if (message instanceof ResponseMessage) {
            return this.handleResponse(message)
        }
        if (message instanceof ErrorMessage) {
            return this.handleError(message)
        }
        if (message instanceof DisconnectMessage) {
            return this.handleDisconnect()
        }
    }

    /**
     * Handle a heartbeat received from the server
     * @param heartbeat {Object}
     */
    handleHeartbeat(heartbeat: HeartbeatMessage) {
        this.queue.ack(heartbeat.lastMessageId)
    }

    /**
     * Handle a notification received from the server
     * @param notification {Object}
     */
    handleNotification(notification: NotificationMessage) {
        var method = this.methods[notification.method]
        if (method !== undefined) {
            const response = method(notification.data)
            if (response !== undefined) {
                // XXX result is ignored as the method was called via
                // an notification. Shouldn't this at least log some warning?
            }
        } else {
            this.signals.error.send(new Exception(
                    'No method for notification: ' + notification.method))
        }
    }

    /**
     * Handle a request received from the server
     * @param request {Object}
     */
    handleRequest(request: RequestMessage) {
        var method = this.methods[request.method]
        if (method !== undefined) {
            let response;
            try {
                response = method(request.data)
            } catch(e) {
                request.sendError(e)
                return
            }
            if (response instanceof Promise) {
                response
                    .then(data => request.sendResponse(data))
                    .catch(data => request.sendError(data))
            } else {
                request.sendResponse(response)
            }
        } else {
            this.signals.error.send(new Exception(
                    'No method for request: ' + request.method))
        }
    }

    /**
     * Handle a response received from the server
     * @param response {Object}
     */
    handleResponse(response: ResponseMessage) {
        var request = this.popRequest(response.requestMessageId)
        request.responseCallback!(response.data)
    }

    /**
     * Handle an error response received from the server.
     * @param error {Object}
     */
    handleError(error: ErrorMessage) {
        var request = this.popRequest(error.requestMessageId)
        if (request.errorCallback === undefined) {
            // No errorCallback given. Send an error signal.
            this.signals.error.send(new RequestFailed(
                    request, error))
        } else {
            request.errorCallback(error.data)
        }
    }

    /**
     * Handle a disconnect message received from the server.
     */
    handleDisconnect() {
        if (this.transport !== undefined) {
            this.transport.stop()
            this.transport = undefined
        }
        this.signals.disconnected.send()
    }
}
