import { RequestMessage, ErrorMessage } from "./messages";

/**
 * Base class for all exceptions.
 */
export class Exception {
    message: string
    constructor(message: string) {
        this.message = message
    }
}

/**
 * This exception is thrown when trying to call an operation on an
 * object which is currently in the wrong state.
 */
export class IllegalState extends Exception {}

/**
 * This exception is sent when an error occurs while trying to
 * connect to the server. e.g. Server not responding, timeout,
 * non 200 status codes, etc.
 */
export class ConnectionError extends Exception {}

/**
 * This exception is sent when the transport negotiation failed.
 */
export class TransportNegotiationFailed extends Exception {}

/**
 * This exception is sent when the authentication failed.
 */
export class AuthenticationFailed extends Exception {}

/**
 * This exception is sent when an error response is received for
 * which no error handler is registered.
 * @param request {Object} request object as sent from the client
 * @param response {Object} error response object as received from
 *                          the server
 */
export class RequestFailed extends Exception {
    request: RequestMessage
    response: ErrorMessage
    constructor(request: RequestMessage, response: ErrorMessage) {
        super('Method call failed: ' + request.method)
        this.request = request;
        this.response = response;
    }
}
