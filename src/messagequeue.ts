import { Exception } from "./exceptions";
import { Message } from "./messages";

/**
 * Message queue class for outbound messages. This class is used to queue
 * outbound messages until they are acknowledged by the server. If the
 * connection is lost before the message has been acknowledged it will be
 * retransmitted as soon as the connection is reestablished.
 */
export class MessageQueue {
    nextMessageId: number
    messages: Message[]
    constructor() {
        this.nextMessageId = 0;
        this.messages = [];
    }
    /**
     * Add message to the queue.
     * @param message {Object} object to be added
     * @returns {Number} message id of the added message
     */
    push(message: Message) {
        if (message.messageId !== undefined) {
            throw new Exception('Message does already have a message id');
        }
        message.messageId = this.nextMessageId;
        this.messages.push(message);
        this.nextMessageId += 1;
        return message.messageId;
    }
    /**
     * Acknowledge messages. This removes all messages with with a
     * messageId less or equal than the given messageId from the queue.
     * @param messageId {Number}
     */
    ack(messageId: number) {
        if (messageId >= this.nextMessageId) {
            throw new Exception('Can not acknowledge a message id that was never part of this queue');
        }
        var i;
        for (i=0; i<this.messages.length; ++i) {
            var message = this.messages[i];
            if (message.messageId === undefined || message.messageId > messageId) {
                // First message found which is not acknowledged.
                break;
            }
        }
        this.messages.splice(0, i);
    }

    /**
     * Call function for every message in the queue.
     * @param f {Function} the function to be called
     */
    forEach(f: (message: Message) => void) {
        for (let message of this.messages) {
            f(message)
        }
    }
}
