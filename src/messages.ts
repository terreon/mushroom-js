import { Client } from "./client"
import { Exception } from "./exceptions";

export enum MessageCode {
    HEARTBEAT=0,
    NOTIFICATION=1,
    REQUEST=2,
    RESPONSE=3,
    ERROR=4,
    DISCONNECT=-1,
}

/**
 * Interface that has to be implemented by the different message types.
 * @interface
 */
export abstract class Message {
    client: Client
    messageId?: number
    constructor(options: { client: Client, messageId?: number }) {
        this.client = options.client
        this.messageId = options.messageId
    }
    abstract toList(): any[]
    static fromList(data: any[], client: Client): Message {
        // FIXME Add some kind of validation here. Right now the
        // Server can send basicly any kind of data causing funky stuff
        // to happen.
        switch (data[0]) {
            case MessageCode.HEARTBEAT:
                return new HeartbeatMessage({
                    client: client,
                    lastMessageId: data[1]
                })
            case MessageCode.NOTIFICATION:
                return new NotificationMessage({
                    client: client,
                    messageId: data[1],
                    method: data[2],
                    data: data[3]
                })
            case MessageCode.REQUEST:
                return new RequestMessage({
                    client: client,
                    messageId: data[1],
                    method: data[2],
                    data: data[3],
                })
            case MessageCode.RESPONSE:
                return new ResponseMessage({
                    client: client,
                    messageId: data[1],
                    requestMessageId: data[2],
                    data: data[3]
                })
            case MessageCode.ERROR:
                return new ErrorMessage({
                    client: client,
                    messageId: data[1],
                    requestMessageId: data[2],
                    data: data[3]
                })
            case MessageCode.DISCONNECT:
                return new DisconnectMessage({
                    client: client
                })
            default:
                throw new Exception(
                        'Unsupported message code: ' + data[0])
        }
    }
}

export class HeartbeatMessage extends Message {
    lastMessageId: number
    constructor(options: { client: Client, lastMessageId: number }) {
        super(options)
        this.lastMessageId = options.lastMessageId
    }
    toList() {
        return [
            MessageCode.HEARTBEAT,
            this.lastMessageId
        ]
    }
}

export class NotificationMessage extends Message {
    method: string
    data: any
    constructor(options: { client: Client, messageId?: number, method: string, data: string }) {
        super(options)
        this.method = options.method
        this.data = options.data
    }
    toList() {
        return [
            MessageCode.NOTIFICATION,
            this.messageId,
            this.method,
            this.data,
        ]
    }
}

export class RequestMessage extends Message {
    method: string
    data: any
    responseCallback: ((data: any) => void) | undefined
    errorCallback: ((data: any) => void) | undefined
    constructor(options: { client: Client, messageId?: number, method: string, data: string, responseCallback?: (data: any) => void, errorCallback?: (data: any) => void }) {
        super(options)
        this.method = options.method
        this.data = options.data
        this.responseCallback = options.responseCallback
        this.errorCallback = options.errorCallback
    }
    toList() {
        return [
            MessageCode.REQUEST,
            this.messageId,
            this.method,
            this.data,
        ]
    }
    sendResponse(data: any) {
        var response = new ResponseMessage({
            client: this.client,
            requestMessageId: this.messageId!,
            data: data
        })
        this.client.queue.push(response)
        this.client.sendMessage(response)
    }
    sendError(data: any) {
        var error = new ErrorMessage({
            client: this.client,
            requestMessageId: this.messageId!,
            data: data
        })
        this.client.queue.push(error)
        this.client.sendMessage(error)
    }
}

export class ResponseMessage extends Message {
    requestMessageId: number
    data: any
    constructor(options: { client: Client, messageId?: number, requestMessageId: number, data: string }) {
        super(options)
	    this.requestMessageId = options.requestMessageId
        this.data = options.data || null
    }
    toList() {
        return [
            MessageCode.RESPONSE,
            this.messageId,
            this.requestMessageId,
            this.data,
        ]
    }
}

export class ErrorMessage extends Message {
    requestMessageId: number
    data: any
    constructor(options: { client: Client, messageId?: number, requestMessageId: number, data: string }) {
        super(options)
	    this.requestMessageId = options.requestMessageId
        this.data = options.data || null
    }
    toList() {
        return [
            MessageCode.ERROR,
            this.messageId,
            this.requestMessageId,
            this.data,
        ]
    }
}

export class DisconnectMessage extends Message {
    toList() {
        return [
            MessageCode.DISCONNECT
        ]
    }
}
