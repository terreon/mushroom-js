import { BaseTransport } from "./basetransport"
import { Client } from "./client"
import { State } from "./state"
import { IllegalState } from "./exceptions"
import { post } from "./xhr"
import { Message } from "./messages";

/**
 * Transport for the HTTP poll protocol.
 */
export class PollTransport extends BaseTransport {
    url: string

    /*
     * @param client The client object which is notified
     *     about inbound messages and state changes of the transport
     * @param options {Object}
     */
    constructor(client: Client, options: { url: string }) {
        super(client)
	    this.url = options.url
    }

    /**
     * Start the poll transport. This causes the transport to send poll
     * requests until either the client or the server sides decides to
     * disconnect or a timeout happens.
     */
    start() {
        if (this.state !== State.DISCONNECTED) {
            throw new IllegalState('Already started')
        }
        this.poll()
    }

    /**
     * Stop the poll transport.
     */
    stop() {
        if (this.state === State.DISCONNECTED) {
            throw new IllegalState('Already stopped')
        }
        this.state = State.DISCONNECTING
    }

    /**
     * Perform a poll request.
     */
    private poll() {
        this.state = State.CONNECTED
        this.client.signals.connected.send()
        // XXX This is the same for all transports - move this logic
        //     into the client.
        this.client.queue.forEach(this.sendMessage.bind(this))
        var request = [
            [0, this.lastMessageId]
        ]
        post(this.url, request)
            .then(xhr => {
                var data = JSON.parse(xhr.responseText)
                for (const messageData of data) {
                    var message = Message.fromList(messageData, this.client)
                    if ('messageId' in message) {
                        if (message.messageId !== undefined &&
                                this.lastMessageId !== undefined &&
                                message.messageId <= this.lastMessageId) {
                            // skip messages which we have already processed
                            return
                        }
                        this.lastMessageId = message.messageId
                    }
                    this.client.handleMessage(message)
                }
                if (this.state === State.DISCONNECTING) {
                    this.state = State.DISCONNECTED
                    this.client.signals.disconnected.send()
                } else {
                    this.poll()
                }
            })
            .catch(_ => {
                this.state = State.DISCONNECTED
                // FIXME calling handleDisconnect is probably wrong as this method
                // is reserved for handling "Disconnect" messages received from the
                // server.
                this.client.handleDisconnect()
                this.client.disconnect()
            })
    }

    /**
     * Send message to the server.
     */
    async sendMessage(message: Message) {
        // FIXME
        // Add a browser timeout so it is possible to send two or more
        // messages at the same time. Also make sure to check if there
        // is already a send request running.
        var request = [
            message.toList()
        ]
        try {
            await post(this.url, request)
            // FIXME remove message from out-queue
        } catch(e) {
            // FIXME log error
        }
    }
}
