export type SignalHandler<T> = (v: T) => void

/**
 * Signal class which allows distribution of signals. This class is
 * used by the mushroom Client class to notify the user code about
 * various state changes.
 */
export class Signal<T> {

    handlers: SignalHandler<T>[]

    constructor() {
        this.handlers = []
    }

    /**
     * Send event to all connected handlers.
     * This function takes any number of arguments which are
     * passed 1:1 to the connected handler functions.
     */
    send(arg: T) {
        for (let handler of this.handlers) {
            handler(arg)
        }
    }

    /**
     * Connect a handler to the signal. The handler will be called with
     * the same arguments that are given to the Signal.send() function.
     */
    connect(handler: SignalHandler<T>) {
        this.handlers.push(handler)
    }

    /**
     * Disconnect handler from the signal. After disconnecting the handler
     * it will no longer receive events from the signal. Calling disconnect
     * on a handler that is not connected is silently ignored.
     */
    disconnect(handler: SignalHandler<T>) {
        var index = this.handlers.indexOf(handler);
        if (index !== -1) {
            this.handlers.splice(index, 1);
        }
    }

    /**
     * Disconnect all handlers.
     */
    disconnectAll() {
        this.handlers.splice(0);
    };
}
