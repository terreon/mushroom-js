/**
 * Constants for the state of the client and its transport.
 * <pre>
 *        .------> [DISCONNECTED] ------.
 *        |              ^   ^          |
 *        |              |   |          |
 * [DISCONNECTING]       |   |      connect()
 *        |              |   |          |
 *        |              |   |          |
 *        |              |   |          v
 *   disconnect()        |   '---- [CONNECTING]
 *        |              |              |
 *        |              |              |
 *        '-------- [CONNECTED] <-------'
 * </pre>
 */
export enum State {
	DISCONNECTED,
	DISCONNECTING,
	CONNECTING,
	CONNECTED,
}
