import { BaseTransport } from "./basetransport";
import { Client } from "./client";
import { State } from "./state";
import { Message, HeartbeatMessage } from "./messages";
import { IllegalState } from "./exceptions";

/**
 * The WebSocket transport.
 */
export class WebSocketTransport extends BaseTransport {
    url: string
    ws: WebSocket | undefined
    /*
     * @param client {mushroom.Client} The client object which is notified
     *     about inbound messages and state changes of the transport
     * @param options {Object}
     */
    constructor(client: Client, options: { url: string }) {
        super(client)
        this.url = options.url
        this.ws = undefined
    }
    start() {
        this.ws = new WebSocket(this.url);
        this.ws.onopen = () => {
            this.state = State.CONNECTED;
            // XXX This is the same for all transports - move this logic
            //     into the client.
            this.client.queue.forEach(this.sendMessage.bind(this));
            this.client.signals.connected.send();
        }
        this.ws.onclose = () => {
            this.state = State.DISCONNECTED;
            this.client.handleDisconnect();
        }
        this.ws.onmessage = (event) => {
            var messageData = JSON.parse(event.data);
            var message = Message.fromList(messageData, this.client);
            if ('messageId' in message) {
                var heartbeat = new HeartbeatMessage({
                    client: this.client,
                    lastMessageId: message.messageId!
                });
                this.sendMessage(heartbeat);
                if (message.messageId !== undefined &&
                        this.lastMessageId !== undefined &&
                        message.messageId <= this.lastMessageId) {
                    // skip messages which we have already processed
                    return;
                }
                this.lastMessageId = message.messageId;
            }
            this.client.handleMessage(message)
        }
    }

    /**
     * Stop the WebSocket transport.
     */
    stop() {
        if (this.ws !== undefined) {
            this.ws.close()
            this.ws = undefined
        }
    }

    /**
     * Send message to the server.
     * @param message {Object}
     */
    sendMessage(message: Message) {
        var data = message.toList();
        var frame = JSON.stringify(data);
        if (this.ws === undefined) {
            throw new IllegalState("WebSocket is not connected")
        }
        this.ws.send(frame);
    }
}
