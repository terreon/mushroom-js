function createXHR() {
	try {
		return new (<any>window).XMLHttpRequest();
	} catch(e) {}
	try {
		return new (<any>window).ActiveXObject('Microsoft.XMLHTTP');
	} catch(e) {}
}

export function post(url: string, data: any) {
    return new Promise<XMLHttpRequest>((resolve, reject) => {
        var xhr = createXHR();
        xhr.open('POST', url, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status == 200) {
                    resolve(xhr)
                } else {
                    reject(xhr)
                }
            }
        };
        /* In order to make so called 'simple requests' that work via CORS
        * the Content-Type is very limited. We simply use text/plain which
        * is better than using form-data content types.
        * https://developer.mozilla.org/en/http_access_control#Simple_requests
        */
        if (data !== null) {
            xhr.setRequestHeader('Content-Type', 'text/plain')
            xhr.send(JSON.stringify(data))
        } else {
            xhr.send(null);
        }
    })
}
